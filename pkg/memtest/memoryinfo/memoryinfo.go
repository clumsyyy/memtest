package memoryinfo

import ()

type MemoryInfo struct {
	MemTotal int64

	MemAvailable int64

	MemFree int64

	Buffers int64

	Cached int64

	Slab int64
}

func (m *MemoryInfo) GetUsedAvailable() float64 {
	return (float64(m.MemAvailable) / float64(m.MemTotal)) * 100
}

func (m *MemoryInfo) GetUsedFree() float64 {
	return (float64(m.MemFree) / float64(m.MemTotal)) * 100
}

func (m *MemoryInfo) GetUsedFreeWithCache() float64 {
	return (float64(m.GetFreeWithBuffersAndCache()) / float64(m.MemTotal)) * 100
}

func (m *MemoryInfo) GetFreeWithBuffersAndCache() int64 {
	return m.MemFree + m.Buffers + m.Cached + m.Slab
}

func (m *MemoryInfo) GetPercentUsedWithMemAvailable() float64 {
	return (float64(m.MemAvailable) / float64(m.MemTotal)) * 100
}

func (m *MemoryInfo) GetPercentUsedWithSar() float64 {
	return (float64(m.GetFreeWithBuffersAndCache()) / float64(m.MemTotal)) * 100
}
