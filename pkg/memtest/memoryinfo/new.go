package memoryinfo

import (
	"os"
	"strconv"
	"strings"
)

func New() (*MemoryInfo, error) {
	data, err := os.ReadFile("/proc/meminfo")
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(data), "\n")

	// line 0 should be memtotal
	fields := strings.Fields(lines[0])
	memTotal, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		return nil, err
	}

	fields = strings.Fields(lines[1])

	memFree, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		return nil, err
	}

	fields = strings.Fields(lines[2])

	memAvail, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		return nil, err
	}

	fields = strings.Fields(lines[3])

	buffers, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		return nil, err
	}

	fields = strings.Fields(lines[4])
	cached, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		return nil, err
	}

	fields = strings.Fields(lines[24])
	slab, err := strconv.ParseInt(fields[1], 10, 64)
	if err != nil {
		return nil, err
	}

	return &MemoryInfo{
		MemTotal:     memTotal,
		MemFree:      memFree,
		MemAvailable: memAvail,
		Buffers:      buffers,
		Cached:       cached,
		Slab:         slab,
	}, nil
}
