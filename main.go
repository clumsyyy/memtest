package main

import (
	"fmt"
	"time"

	mi "memtest/pkg/memtest/memoryinfo"
)

func main() {

	for {
		info, err := mi.New()
		if err != nil {
			fmt.Println(err)
			continue
		}

		fmt.Printf("MemTotal = %d\n", info.MemTotal)
		kbmemused := info.GetFreeWithBuffersAndCache()

		fmt.Printf("kbmemused available = %d\n", kbmemused)
		fmt.Printf("meminfo MemoryAvailable = %d\n", info.MemAvailable)

		fmt.Printf("Percent free with MemAvailable = %0.2f\n", info.GetPercentUsedWithMemAvailable())
		fmt.Printf("Percent free with sar = %0.2f\n", info.GetPercentUsedWithSar())

		fmt.Printf("\n\n")
		time.Sleep(time.Second * 1)
	}
}
