# Calculating memory usage

use sar -r 1 to print memory statistics each second, this is used to 

### analysis scripts are using the %memused column which = the kbmemused column divided by total memory in KB
* memory total in KB is not available through sar, have to get through /proc/meminfo


### How to get kbmemused column
kbmemused = total - kbmemfree - kbuffers - kbcached - kbslab


kbmemused= struct stats_memory.frmkb field
% memory used = total - (frmkb + bufkb + camkb + slabkb)

kbmemused = MemTotal - (MemFree + Buffers + Cached + Slab)


## To confirm
### sysstat/rd_stats.h
Contains the struct stats_memory 

### sysstat/rd_stats.c
Function read_meminfo() reads /proc/meminfo fields and populates a stats_memory struct using the fields

### sysstat/pr_stats.c
* Prints the struct stats_memory object
* calculates kbmemused as total memory minus "nousedmem" variable 


## Git commit of MemAvailable field being added along with Linux Torvalds explanation
https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=34e431b0ae398fc54ea69ff85ec700722c9da773


## MemAvailable calculations
MemAvailable = MemFree + Active(file) + Inactive(file) + SReclaimable








